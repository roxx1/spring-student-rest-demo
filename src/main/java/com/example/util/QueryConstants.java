package com.example.util;

public final class QueryConstants {

    private QueryConstants() {
        throw new AssertionError();
    }

    public static final String CATEGORIES = "categories";
    public static final String ACTIVE = "active";
    public static final String ADMISSION_YEAR_AFTER = "admissionYearAfter";
    public static final String ADMISSION_YEAR_BEFORE = "admissionYearBefore";
    public static final String PAGE_NO = "pageNo";
    public static final String PAGE_SIZE = "pageSize";
}
