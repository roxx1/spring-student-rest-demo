package com.example.mapper;

import com.example.persistence.model.Student;
import com.example.web.dto.StudentDto;
import com.google.common.base.Preconditions;

public final class StudentMapper {

    private StudentMapper() {
        throw new AssertionError();
    }

    public static StudentDto convertStudentToStudentDto(final Student student) {
        Preconditions.checkNotNull(student);
        final StudentDto studentDto = new StudentDto(student.getName(), student.getCategory(), student.getActive(), student.getAdmissionDate());
        return studentDto;
    }

    public static Student convertStudentDtoToStudent(final StudentDto studentDto) {
        Preconditions.checkNotNull(studentDto);
        final Student student = new Student(studentDto.getName(), studentDto.getCategory(), studentDto.isActive(), studentDto.getAdmissionYear());
        return student;
    }

}
