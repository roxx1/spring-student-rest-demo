package com.example.service;

import com.example.web.dto.CategoryDto;
import com.example.web.dto.StudentDto;

import java.util.Date;
import java.util.List;

public interface StudentService {

    void createStudent(final StudentDto studentDto);

    List<StudentDto> getAllStudents(final String categories, final Boolean active, final Date admissionYearAfter, final Date admissionYearBefore, final Integer pageNo, final Integer pageSize);

    StudentDto fetchStudent(final Long id);

    void deleteStudent(final Long id);

    void updateCategory(final Long id, final CategoryDto categoryDto);

}
