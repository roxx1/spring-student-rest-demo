package com.example.service;


import com.example.exception.MyEntityNotFoundException;

public final class ServicePreconditions {

    private ServicePreconditions() {
        throw new AssertionError();
    }

    public static <T> void checkEntityExists(final T entity) {
        checkEntityExists(entity, null);
    }

    public static <T> void checkEntityExists(final T entity, final String message) {
        if (entity == null) {
            throw new MyEntityNotFoundException(message);
        }
    }

}
