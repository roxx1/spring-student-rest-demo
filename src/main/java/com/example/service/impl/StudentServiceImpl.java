package com.example.service.impl;

import com.example.mapper.StudentMapper;
import com.example.persistence.model.Student;
import com.example.persistence.repository.StudentRepository;
import com.example.service.ServicePreconditions;
import com.example.service.StudentService;
import com.example.web.dto.CategoryDto;
import com.example.web.dto.StudentDto;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public void createStudent(final StudentDto studentDto) {
        final Student student = StudentMapper.convertStudentDtoToStudent(studentDto);
        studentRepository.save(student);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudentDto> getAllStudents(final String categories, final Boolean active, final Date admissionYearAfter, final Date admissionYearBefore, final Integer pageNo, final Integer pageSize) {
        final List<Student> students = studentRepository.getAllStudents(getCategoryList(categories), active, admissionYearAfter, admissionYearBefore, pageNo, pageSize);

        if (students == null) {
            return Lists.newArrayList();
        }

        return students.stream().map(s -> StudentMapper.convertStudentToStudentDto(s)).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public StudentDto fetchStudent(final Long id) {
        final Student student = findStudentExists(id);
        return StudentMapper.convertStudentToStudentDto(student);
    }

    @Override
    public void deleteStudent(final Long id) {
        final Student student = findStudentExists(id);
        studentRepository.delete(student);
    }

    @Override
    public void updateCategory(final Long id, final CategoryDto categoryDto) {
        final Student student = findStudentExists(id);
        student.setCategory(categoryDto.getCategory());
        studentRepository.update(student);
    }


    public Student findStudentExists(final Long id) {
        final Student student = studentRepository.findOne(id);
        ServicePreconditions.checkEntityExists(student);
        return student;
    }

    public List<Integer> getCategoryList(String categories) {
        final String[] stringCategories = categories.split(",");
        return Arrays.stream(stringCategories).map(s -> Integer.parseInt(s)).collect(Collectors.toList());
    }

}
