package com.example.exception;

public class MyEntityNotFoundException extends RuntimeException {

    public MyEntityNotFoundException() {
        super();
    }

    public MyEntityNotFoundException(String message) {
        super(message);
    }

    public MyEntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyEntityNotFoundException(Throwable cause) {
        super(cause);
    }
}
