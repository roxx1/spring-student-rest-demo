package com.example.exception;

public class MyBadRequestException extends RuntimeException {

    public MyBadRequestException(String message) {
    }

    public MyBadRequestException() {
        super();
    }

    public MyBadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyBadRequestException(Throwable cause) {
        super(cause);
    }
}
