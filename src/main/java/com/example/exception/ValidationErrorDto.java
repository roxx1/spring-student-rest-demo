package com.example.exception;

import com.google.common.collect.Lists;

import java.util.List;

public class ValidationErrorDto {

    private final List<FieldErrorDto> list = Lists.newArrayList();

    public final void addFieldErrorDto(final String field,final String message) {
        final FieldErrorDto errorDto = new FieldErrorDto(field,message);
        list.add(errorDto);
    }

    public final List<FieldErrorDto> getList() {
        return list;
    }

    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ValidationErrorDTO [fieldErrors=").append(list).append("]");
        return builder.toString();
    }
}
