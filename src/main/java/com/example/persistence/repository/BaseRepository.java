package com.example.persistence.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

public abstract class BaseRepository<T extends Serializable> implements IGenericRepository<T> {

    @PersistenceContext
    protected EntityManager em;

    private Class<T> clazz;

    public final void setClazz(final Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public List<T> findAll() {
        return em.createQuery("from " + clazz.getName()).getResultList();
    }

    @Override
    public void save(final T entity) {
        em.persist(entity);
    }

    @Override
    public T findOne(final long id) {
        return em.find(clazz, id);
    }

    @Override
    public T update(final T entity) {
        return em.merge(entity);
    }

    @Override
    public void delete(final T entity) {
        em.remove(entity);
    }

    @Override
    public void deleteById(final long entityId) {
        final T entity = findOne(entityId);
        delete(entity);
    }

}
