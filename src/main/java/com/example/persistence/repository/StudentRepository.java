package com.example.persistence.repository;

import com.example.persistence.model.Student;

import java.util.Date;
import java.util.List;

public interface StudentRepository extends IGenericRepository<Student> {
    List<Student> getAllStudents(final List<Integer> categories, final Boolean active, final Date admissionYearAfter, final Date admissionYearBefore, final Integer pageNo, final Integer pageSize);
}
