package com.example.persistence.repository;

import java.io.Serializable;
import java.util.List;

public interface IGenericRepository<T extends Serializable> {
    
    T findOne(final long id);

    List<T> findAll();

    void save(final T entity);

    T update(final T entity);

    void delete(final T entity);

    void deleteById(final long entityId);
}
