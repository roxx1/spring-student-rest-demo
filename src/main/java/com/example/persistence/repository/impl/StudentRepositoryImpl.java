package com.example.persistence.repository.impl;

import com.example.persistence.model.Student;
import com.example.persistence.repository.BaseRepository;
import com.example.persistence.repository.StudentRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

@Repository
public class StudentRepositoryImpl extends BaseRepository<Student> implements StudentRepository {

    public StudentRepositoryImpl() {
        setClazz(Student.class);
    }

    @Override
    public List<Student> getAllStudents(final List<Integer> categories, final Boolean active, final Date admissionYearAfter, final Date admissionYearBefore, final Integer pageNo, final Integer pageSize) {
        final CriteriaBuilder builder = em.getCriteriaBuilder();
        final CriteriaQuery<Student> cr = builder.createQuery(Student.class);
        final Root<Student> root = cr.from(Student.class);
        cr.select(root);
        final List<Predicate> predicates = Lists.newArrayList();

        if (categories != null && !categories.isEmpty()) {
            predicates.add(root.get("category").in(categories));
        }

        if (active != null) {
            predicates.add(builder.equal(root.get("active"), active));
        }

        if (admissionYearAfter != null) {
            predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("admissionDate"), admissionYearAfter));
        }

        if (admissionYearBefore != null) {
            predicates.add(builder.lessThanOrEqualTo(root.<Date>get("admissionDate"), admissionYearBefore));
        }

        cr.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

        final TypedQuery<Student> query = em.createQuery(cr);

        if (pageNo != null && pageSize != null) {
            query.setFirstResult(pageNo);
            query.setMaxResults(pageSize);
        }

        return query.getResultList();
    }
}
