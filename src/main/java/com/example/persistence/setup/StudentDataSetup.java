package com.example.persistence.setup;

import com.example.persistence.model.Student;
import com.example.persistence.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

@Component
public class StudentDataSetup implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent contextRefreshedEvent) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();

        Student s1 = new Student();
        s1.setName("Abc");
        s1.setCategory(1);
        calendar.set(Calendar.YEAR, 2018);
        s1.setAdmissionDate(calendar.getTime());
        s1.setActive(Boolean.TRUE);

        Student s2 = new Student();
        s2.setName("Def");
        s2.setCategory(2);
        calendar.clear();
        calendar.set(Calendar.YEAR, 2016);
        s2.setAdmissionDate(calendar.getTime());
        s2.setActive(Boolean.FALSE);

        Student s3 = new Student();
        s3.setName("Ghi");
        s3.setCategory(1);
        calendar.clear();
        calendar.set(Calendar.YEAR, 2014);
        s3.setAdmissionDate(calendar.getTime());
        s3.setActive(Boolean.TRUE);

        studentRepository.save(s1);
        studentRepository.save(s2);
        studentRepository.save(s3);
    }
}
