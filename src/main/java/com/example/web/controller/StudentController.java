package com.example.web.controller;

import com.example.service.StudentService;
import com.example.util.QueryConstants;
import com.example.web.RestPreconditions;
import com.example.web.dto.CategoryDto;
import com.example.web.dto.StudentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("students")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentDto> fetchAllStudents(@RequestParam(value = QueryConstants.CATEGORIES, required = false) final String categories, @RequestParam(value = QueryConstants.ACTIVE, required = false) final Boolean active, @RequestParam(value = QueryConstants.ADMISSION_YEAR_AFTER, required = false) @DateTimeFormat(pattern = "yyyy") final Date admissionYearAfter, @RequestParam(value = QueryConstants.ADMISSION_YEAR_BEFORE, required = false) @DateTimeFormat(pattern = "yyyy") final Date admissionYearBefore, @RequestParam(value = QueryConstants.PAGE_NO, required = false) final Integer pageNo, @RequestParam(value = QueryConstants.PAGE_SIZE, required = false) final Integer pageSize) {
        return studentService.getAllStudents(categories, active, admissionYearAfter, admissionYearBefore, pageNo, pageSize);
    }

    @GetMapping("students/{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentDto fetchStudent(@PathVariable("id") final Long id) {
        return studentService.fetchStudent(id);
    }

    @PostMapping("students")
    @ResponseStatus(HttpStatus.CREATED)
    public void createStudent(@RequestBody @Valid final StudentDto studentDto) {
        RestPreconditions.checkRequestElementNotNull(studentDto);
        studentService.createStudent(studentDto);
    }

    @PatchMapping("students/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCategory(@PathVariable("id") final Long id, @RequestBody @Valid final CategoryDto categoryDto) {
        RestPreconditions.checkRequestElementNotNull(categoryDto);
        studentService.updateCategory(id, categoryDto);
    }

    @DeleteMapping("students/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteStudent(@PathVariable("id") final Long id) {
        studentService.deleteStudent(id);
    }

}
