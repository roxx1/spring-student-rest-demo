package com.example.web;

import com.example.exception.MyBadRequestException;

public final class RestPreconditions {

    private RestPreconditions() {
        throw new AssertionError();
    }

    public static <T> void checkRequestElementNotNull(final T entity) {
        checkRequestElementNotNull(entity, null);
    }


    public static <T> void checkRequestElementNotNull(final T entity, final String message) {
        if (entity == null) {
            throw new MyBadRequestException(message);
        }
    }
}
