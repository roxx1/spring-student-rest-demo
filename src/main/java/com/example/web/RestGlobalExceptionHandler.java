package com.example.web;

import com.example.exception.ApiError;
import com.example.exception.MyEntityNotFoundException;
import com.example.exception.ValidationErrorDto;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestControllerAdvice
public class RestGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    // 400

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final ApiError body = message(HttpStatus.BAD_REQUEST, ex);
        return handleExceptionInternal(ex, body, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final BindingResult bindingResult = ex.getBindingResult();
        final List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        final ValidationErrorDto dto = processFieldErrors(fieldErrors);
        return handleExceptionInternal(ex, dto, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({ConstraintViolationException.class, DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleBadRequest(final RuntimeException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.BAD_REQUEST, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    // 403

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<Object> handleForbiddenException(final AccessDeniedException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.FORBIDDEN, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    // 404

    @ExceptionHandler({MyEntityNotFoundException.class, EntityNotFoundException.class})
    public ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.NOT_FOUND, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }


    // 409

    @ExceptionHandler({InvalidDataAccessApiUsageException.class, DataAccessException.class})
    public ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.CONFLICT, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    // 415

    @ExceptionHandler({InvalidMediaTypeException.class, InvalidMimeTypeException.class})
    public ResponseEntity<Object> handleInvalidMediaType(final RuntimeException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.UNSUPPORTED_MEDIA_TYPE, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.UNSUPPORTED_MEDIA_TYPE, request);
    }

    // 500

    @ExceptionHandler({NullPointerException.class, IllegalStateException.class, IllegalArgumentException.class})
    public ResponseEntity<Object> handleServerError(final RuntimeException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.INTERNAL_SERVER_ERROR, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }


    private ValidationErrorDto processFieldErrors(final List<FieldError> fieldErrors) {
        final ValidationErrorDto dto = new ValidationErrorDto();
        if (fieldErrors != null) {
            for (FieldError fieldError : fieldErrors) {
                dto.addFieldErrorDto(fieldError.getField(), fieldError.getDefaultMessage());
            }
        }
        return dto;
    }

    private ApiError message(final HttpStatus status, final Exception ex) {
        final String message = ex.getMessage();
        final String devMessage = ExceptionUtils.getRootCauseMessage(ex);
        final ApiError apiError = new ApiError(status.value(), message, devMessage);
        return apiError;
    }
}
