package com.example.web.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class CategoryDto implements Serializable {

    @NotNull
    private Integer category;

    public CategoryDto() {
    }

    public CategoryDto(int category) {
        this.category = category;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
