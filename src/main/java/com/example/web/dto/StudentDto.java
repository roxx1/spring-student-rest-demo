package com.example.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class StudentDto implements Serializable {

    @NotBlank
    private String name;

    @NotNull
    private Integer category;

    private Boolean active;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT, pattern = "yyyy")
    private Date admissionYear;

    public StudentDto() {
        this.active = true;
    }

    public StudentDto(final String name, final Integer category, final Boolean active, final Date admissionYear) {
        this.name = name;
        this.category = category;
        this.active = active;
        this.admissionYear = admissionYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getAdmissionYear() {
        return admissionYear;
    }

    public void setAdmissionYear(Date admissionYear) {
        this.admissionYear = admissionYear;
    }
}
